<?php

$exports["log"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    echo("\n");
    return [];
  };
};

$exports["warn"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    echo("\n");
    return [];
  };
};

$exports["error"] = function ($s) {
  return function () use (&$s) {
    die($s);
    echo("\n");
    return [];
  };
};

$exports["info"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    echo("\n");
    return [];
  };
};
